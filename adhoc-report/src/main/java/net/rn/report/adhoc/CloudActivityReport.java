package net.rn.report.adhoc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 * 
 */
public class CloudActivityReport {
    private static final Logger LOGGER = LoggerFactory.getLogger(CloudActivityReport.class);
    private static final String query = "select cloud_name, cloud_number, email, phone, ec.created_date as cloud_creation_date, "
            + "null as last_migration_time,null as last_chat_time, null as last_connection_request_time from cloud_name cn, "
            + "entity_cloud ec where ec.entity_cloud_id = cn.entity_cloud_id and guardian_id is null union all select ANY_VALUE(cloud_name), "
            + "ANY_VALUE(cloud_number), null, null, null, max(mu.mgr_time) as migration_time, null, null from cloud_name cn,"
            + " entity_cloud ec, migration_update mu where ec.entity_cloud_id = cn.entity_cloud_id and guardian_id is null and mu.mgr_cloud_number = ec.cloud_number "
            + "group by cloud_number union all (select null, message_by, null, null, null, null, MAX(created_time), null from chat_history, "
            + "entity_cloud where message_by = cloud_number and guardian_id is null group by message_by) union all select null,"
            + "cr.requesting_cloud_number , null, null, null, null, null, MAX(creation_date) from connection_request cr, entity_cloud ec where "
            + "cr.requesting_cloud_number = ec.cloud_number and guardian_id is null group by cr.requesting_cloud_number";

    public static void main(String[] args) {
        LOGGER.info("Clouds Activities report START");
        generateReport();
        LOGGER.info("Clouds Activities report END");
    }

    /**
     * 
     */
    private static void generateReport() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pcloud_local", "root", "root");
            Statement stmt = con.createStatement();
            LOGGER.info("Going to execute query: {}", query);
            ResultSet rs = stmt.executeQuery(query);
            Map<String, ActivityReport> cloudReport = new HashMap<String, ActivityReport>();
            while (rs.next()) {
                String cloudName = rs.getString("cloud_name");
                String cloudNumber = rs.getString("cloud_number");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                String cloudCreationDate = rs.getString("cloud_creation_date");
                String migrationTime = rs.getString("last_migration_time");
                String lastChatTime = rs.getString("last_chat_time");
                String lastConnReqTime = rs.getString("last_connection_request_time");
                System.out.println("email : " + email + " phone: " + phone);
                ActivityReport activityReport = null;
                if (cloudReport.get(cloudNumber) == null) {
                    activityReport = new CloudActivityReport().new ActivityReport();
                    if (cloudName != null) {
                        activityReport.setCloudName(cloudName);
                    }
                    if (cloudNumber != null) {
                        activityReport.setCloudNumber(cloudNumber);
                    }
                    if (email != null && !(" ").equals(email) && !("null").equals(email)) {
                        activityReport.setEmail(email);
                    }
                    if (phone != null && !(" ").equals(phone) && !("null").equals(phone)) {
                        activityReport.setPhone(phone);
                    }
                    if (cloudCreationDate != null) {
                        activityReport.setCloudCreationDate(cloudCreationDate);
                    }
                    if (migrationTime != null) {
                        activityReport.setMigrationTime(migrationTime);
                    }
                    if (lastChatTime != null) {
                        activityReport.setLastChatTime(lastChatTime);
                    }
                    if (lastConnReqTime != null) {
                        activityReport.setLastConnReqTime(lastConnReqTime);
                    }
                    cloudReport.put(cloudNumber, activityReport);

                } else {
                    activityReport = cloudReport.get(cloudNumber);
                    if (activityReport != null && activityReport.getCloudNumber() == null) {
                        activityReport.setCloudNumber(cloudNumber);
                    }
                    if (activityReport != null && activityReport.getCloudName() == null) {
                        activityReport.setCloudName(cloudName);
                    }
                    if (activityReport != null && email == null && email == "null") {
                        activityReport.setEmail(email);
                    }
                    if (activityReport != null && phone == null && phone == "null") {
                        activityReport.setPhone(phone);
                    }
                    if (activityReport != null && activityReport.getCloudCreationDate() == null) {
                        activityReport.setCloudCreationDate(cloudCreationDate);
                    }
                    if (activityReport != null && activityReport.getMigrationTime() == null) {
                        activityReport.setMigrationTime(migrationTime);
                    }
                    if (activityReport != null && activityReport.getLastChatTime() == null) {
                        activityReport.setLastChatTime(lastChatTime);
                    }
                    if (activityReport != null && activityReport.getLastConnReqTime() == null) {
                        activityReport.setLastConnReqTime(lastConnReqTime);
                    }
                }

            }
            writeReport(cloudReport);
        } catch (ClassNotFoundException e1) {
            LOGGER.error("An error Occured: {}", e1.getMessage(), e1);
        } catch (SQLException e) {
            LOGGER.error("An error Occured: {}", e.getMessage(), e);
        }
    }

    private static void writeReport(Map<String, ActivityReport> cloudReport) {
        LOGGER.info("Write report to xls file START.");
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedReader br = null;

        XSSFWorkbook workbook = null;
        XSSFSheet sheet = null;
        try {
            LOGGER.info("Loading property file: report.properties");
            Properties props = new Properties();
            props.load(new FileInputStream(new File("report.properties")));

            String pathToWrite = props.getProperty("report.write.path");
            LOGGER.info("Write File to path: {}", pathToWrite);
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet("Cloud_Activity_Report");
            Row row = sheet.createRow(0);
            int columnCount = 0;
            row.createCell(columnCount++).setCellValue("Cloud Name");
            row.createCell(columnCount++).setCellValue("Cloud Number");
            row.createCell(columnCount++).setCellValue("email");
            row.createCell(columnCount++).setCellValue("phone");
            row.createCell(columnCount++).setCellValue("Cloud Creation Date");
            row.createCell(columnCount++).setCellValue("Cloud Migration Date");
            row.createCell(columnCount++).setCellValue("Cloud Last Chat Time");
            row.createCell(columnCount++).setCellValue("Cloud Last Connection Request Time");

            int rowCount = sheet.getPhysicalNumberOfRows();

            LOGGER.info("Start reading log file");
            for (ActivityReport activityReport : cloudReport.values()) {
                Row row1 = sheet.createRow(rowCount++);
                int columnCount1 = 0;
                row1.createCell(columnCount1++).setCellValue(activityReport.getCloudName());
                row1.createCell(columnCount1++).setCellValue(activityReport.getCloudNumber());
                row1.createCell(columnCount1++).setCellValue(activityReport.getEmail());
                row1.createCell(columnCount1++).setCellValue(activityReport.getPhone());
                row1.createCell(columnCount1++).setCellValue(activityReport.getCloudCreationDate());
                row1.createCell(columnCount1++).setCellValue(activityReport.getMigrationTime());
                if (activityReport.getLastChatTime() != null) {
                    row1.createCell(columnCount1++).setCellValue(
                            Instant.ofEpochMilli(Long.parseLong(activityReport.getLastChatTime())).toString());
                }
                row1.createCell(columnCount1++).setCellValue(activityReport.getLastConnReqTime());
            }
            fileOutputStream = new FileOutputStream(pathToWrite);
            workbook.write(fileOutputStream);

        } catch (Exception ex) {
            LOGGER.error("Exception in LogReaderServiceImpl: {}", ex.getMessage(), ex);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fileOutputStream != null)
                    fileOutputStream.close();
            } catch (IOException io) {
                LOGGER.error("Exception while closing resources in LogReaderServiceImpl: {}", io.getMessage(), io);
            }
        }
        LOGGER.info("Report generated successfully.");
    }

    public class ActivityReport {

        String cloudName;
        String cloudNumber;
        String email;
        String phone;
        String cloudCreationDate;
        String migrationTime;
        String lastChatTime;
        String lastConnReqTime;

        /**
         * @return the cloudName
         */
        public String getCloudName() {
            return cloudName;
        }

        /**
         * @param cloudName
         *            the cloudName to set
         */
        public void setCloudName(String cloudName) {
            this.cloudName = cloudName;
        }

        /**
         * @return the cloudNumber
         */
        public String getCloudNumber() {
            return cloudNumber;
        }

        /**
         * @param cloudNumber
         *            the cloudNumber to set
         */
        public void setCloudNumber(String cloudNumber) {
            this.cloudNumber = cloudNumber;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email
         *            the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return the phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         * @param phone
         *            the phone to set
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         * @return the cloudCreationDate
         */
        public String getCloudCreationDate() {
            return cloudCreationDate;
        }

        /**
         * @param cloudCreationDate
         *            the cloudCreationDate to set
         */
        public void setCloudCreationDate(String cloudCreationDate) {
            this.cloudCreationDate = cloudCreationDate;
        }

        /**
         * @return the migrationTime
         */
        public String getMigrationTime() {
            return migrationTime;
        }

        /**
         * @param migrationTime
         *            the migrationTime to set
         */
        public void setMigrationTime(String migrationTime) {
            this.migrationTime = migrationTime;
        }

        /**
         * @return the lastChatTime
         */
        public String getLastChatTime() {
            return lastChatTime;
        }

        /**
         * @param lastChatTime
         *            the lastChatTime to set
         */
        public void setLastChatTime(String lastChatTime) {
            this.lastChatTime = lastChatTime;
        }

        /**
         * @return the lastConnReqTime
         */
        public String getLastConnReqTime() {
            return lastConnReqTime;
        }

        /**
         * @param lastConnReqTime
         *            the lastConnReqTime to set
         */
        public void setLastConnReqTime(String lastConnReqTime) {
            this.lastConnReqTime = lastConnReqTime;
        }

    }
}
